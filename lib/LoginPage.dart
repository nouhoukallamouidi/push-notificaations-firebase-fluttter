import 'package:flutter/material.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';



import 'WebViewContainer.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  // Initialisez SharedPreferences
  late SharedPreferences _prefs;


  @override
  void initState() {
    super.initState();
   //_initPrefs();
   _initPref();

  }
  _initPref() async {
    _prefs = await SharedPreferences.getInstance();
  }

  // Fonction pour initialiser SharedPreferences
  _initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
    // Vérifiez si l'utilisateur est déjà connecté
    bool isLoggedIn = _prefs.getBool('isLoggedIn') ?? false;
    if (isLoggedIn) {
      // Si l'utilisateur est connecté, accédez directement à WebViewContainer
      String? userEmail = _prefs.getString('userEmail');
      String? urlWebView = _prefs.getString('urlWebView');
      if (userEmail != null) {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => WebViewContainer(user_mail: userEmail, url_web: urlWebView,),
          ),
        );
      }
    }
  }

  String hashPassword(String password) {
    final salt = "telus";
     final bytes = utf8.encode(password + salt);
    final hashedBytes = sha256.convert(bytes);
    return hashedBytes.toString();
  }


  Future<void> login() async {
    final password = passwordController.text;
    final digest = hashPassword(password);

    print(digest.toString());

    final response = await http.post(
      Uri.parse('https://telusholding.cloud/Mobile_test/fetch2.php'), // Remplacez par l'URL de votre API
      body: {
        'email': emailController.text,
        'passwd':  digest,
      },
    );
    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);
      final message = responseData['message'];
      final url = responseData['url'];
      final encodedUrl = Uri.decodeFull(url);
      //final encodedUrl = 'https://www.google.com';
      print(encodedUrl);
      print(responseData);

      if (message == 'OK') {
        _prefs.setBool('isLoggedIn', true);
        _prefs.setString('userEmail', emailController.text);
        _prefs.setString('urlWebView', encodedUrl);

        // Connexion réussie, redirigez l'utilisateur vers la page d'accueil
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => WebViewContainer( user_mail: emailController.text, url_web: encodedUrl ),
          ),
        );
      } else {
        // Échec de l'authentification, affichez un message d'erreur
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Erreur d\'authentification'),
              content: Text('Les informations d\'identification sont incorrectes.'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'),
                ),
              ],
            );
          },
        );
      }
    }else {
      // Un problème est survenu avec la requête HTTP
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Erreur HTTP'),
            content: Text('Un problème est survenu avec la requête HTTP. Veuillez réessayer ultérieurement.'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Connexion'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: emailController,
              decoration: InputDecoration(labelText: 'email'),
            ),
            TextField(
              controller: passwordController,
              decoration: InputDecoration(labelText: 'Mot de passe'),
              obscureText: true,
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: login,
              child: Text('Se connecter'),
            ),
          ],
        ),
      ),
    );
  }
}


