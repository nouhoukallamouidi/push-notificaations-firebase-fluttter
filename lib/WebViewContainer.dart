import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;


class WebViewContainer extends StatefulWidget {
  final String user_mail;
  final url_web;

  WebViewContainer({required this.user_mail, required this.url_web});

  @override
  _WebViewContainerState createState() => _WebViewContainerState();
}

class _WebViewContainerState extends State<WebViewContainer> {
  late WebViewController controller;


  static const String URL_SOKECT_IO = "https://96dd-105-67-7-123.ngrok-free.app/";
  late io.Socket socket;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  String? user_token;
  int notificationId = 0;


  @override
  void initState() {
    super.initState();
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(Uri.parse(widget.url_web));

    // Initialisation du plugin flutter_local_notifications
    const AndroidInitializationSettings androidInitializationSettings =
    AndroidInitializationSettings('@drawable/ic_launcher'); // Remplacez 'app_icon' par le nom de votre icône d'application
    final InitializationSettings initializationSettings =
    InitializationSettings(android: androidInitializationSettings);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);


    // Gestion des notifications en premier plan (l'application est ouverte)
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      // Gérer la notification lorsque l'application est en cours d'exécution (en premier plan)
      print("Notification reçue en premier plan : ${message}");
      // Affichez une notification locale ici
      const AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
        'push_notification_demo', // ID de votre canal de notification
        'localnotification', // Nom de votre canal de notification
        //'petite demo de notification push ', //  de votre canal de notification
        importance: Importance.max,
        priority: Priority.high,
      );
      const NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);

      // Affichez la notification avec le titre et le corps du message
      flutterLocalNotificationsPlugin.show(
        notificationId++, // ID de la notification
        message.notification?.title ?? 'Notification',
        message.notification?.body ?? 'Contenu de la notification',
        platformChannelSpecifics,
      );
    });
    // Initialisez Firebase Messaging
    _firebaseMessaging.getToken().then((token) {
      user_token = token; // Mettez à jour l'état avec le nouveau token
      connect(user_token!);
      print("FCM Token: $token");
      // Connectez-vous au serveur Socket.IO et envoyez le token FCM et l'adresse e-mail
    });

    requestNotificationPermissions();
  }

  Future<void> connect(String token) async   {
    try {
      socket = io.io('https://bc56-105-66-132-39.ngrok-free.app', <String, dynamic>{
        "transports": ["websocket"],
        "autoConnect": false,
      });

        print("encours");
        socket!.connect();
        print('milieu');
        socket!.emit('user_connection', {
          'email': widget.user_mail,
          'token': token,
        });
      print('Connexion établie avec succès');

    } catch (e) {
      print("Error: $e");
    }
  }

  Future<void> requestNotificationPermissions() async {
    final settings = await _firebaseMessaging.requestPermission(
      alert: true, // Autoriser les alertes de notification
      badge: true, // Autoriser les badges de notification
      sound: true, // Autoriser les sons de notification
      provisional: false, // Autorisation provisoire (iOS 12+)
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('Autorisations de notification accordées');
    } else {
      print('Autorisations de notification refusées');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // Autres widgets si nécessaire...
          SizedBox(height: 30.0),
          // WebView prendra l'espace restant dans la colonne
          Expanded(
            child: WebViewWidget(controller: controller),
          ),
        ],
      ),
    );
  }


  // Fonction pour effectuer la vérification des notifications à intervalles réguliers
  void startNotificationCheckLoop() {
    const duration = Duration(seconds: 10); // Intervalle de vérification (10 secondes)
    Timer.periodic(duration, (timer) async {
      // Effectuez une requête pour vérifier s'il y a de nouvelles notifications
      final response = await http.post(
        Uri.parse('https://telusholding.cloud/Mobile_test/notif.php'), // Remplacez par l'URL de votre API
        body: {
          'email': widget.user_mail,
        },
      );
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        final message = responseData['message'];
        if (message == 'vide') {
          // Connexion réussie, redirigez l'utilisateur vers la page d'accueil
          print("vide");

        } else {
          //print(responseData);
          sendNotificationsToFCM(message);
        }
        // Analysez la réponse JSON pour obtenir de nouvelles notifications
        //final notifications = parseNotifications(response.body);

        // Traitez les nouvelles notifications, par exemple, envoyez-les via FCM
        //sendNotificationsToFCM(notifications);
      }
    });
  }

  void sendNotificationsToFCM(String message) async {
    //print (message);
    if (message == null) {
      print("Le message est null, impossible d'envoyer la notification.");
      return;
    }

    // Créez un objet `Map` pour les informations de la notification.
    final notificationData = {
      'title': 'Nouvelle notification',
      'body': message,
    };
    try {
      // Envoyez la notification à l'aide de la méthode `sendMessage()`.
      //final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
      await _firebaseMessaging.sendMessage(
        to: user_token,
        data: notificationData,
      );

      print("Notification envoyée avec succès : $message");
    } catch (error) {
      print("Erreur lors de l'envoi de la notification : $error");
    }
  }



}



