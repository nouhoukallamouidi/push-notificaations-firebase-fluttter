import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LoginPage.dart';
import 'WebViewContainer.dart';




void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
// Initialisation de Firebase Messaging


  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FutureBuilder<Map<String, String?>>(
        // Vérifiez si l'utilisateur est déjà connecté en appelant _initPrefs
        future: _initPrefs(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final userEmail = snapshot.data?['userEmail'];
            final urlWebView = snapshot.data?['urlWebView'];

            if (userEmail != null && urlWebView != null) {
              // Utilisateur déjà connecté, affichez WebViewContainer avec les paramètres
              return WebViewContainer(user_mail: userEmail, url_web: urlWebView);
            } else {
              // Utilisateur non connecté, affichez l'écran de connexion
              return LoginPage();
            }
          } else {
            // Affichez un indicateur de chargement pendant la vérification
            return CircularProgressIndicator();
          }
        },
      ),

    );
  }
}

Future<Map<String, String?>> _initPrefs() async {
  final _prefs = await SharedPreferences.getInstance();
  // Vérifiez si l'utilisateur est déjà connecté
  bool isLoggedIn = _prefs.getBool('isLoggedIn') ?? false;
  if (isLoggedIn) {
    String? userEmail = _prefs.getString('userEmail');
    String? urlWebView = _prefs.getString('urlWebView');
    return {'userEmail': userEmail, 'urlWebView': urlWebView};
  } else {
    return {'userEmail': null, 'urlWebView': null};
  }
}






