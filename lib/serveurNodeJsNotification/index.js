const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);
const io = require("socket.io")(server);
const axios = require("axios");

// FCM
const admin = require('firebase-admin');
const serviceAccount = require('./config/fcmKey.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const cron = require('node-cron');


let userTokens = []; // Tableau pour stocker les tokens FCM
let userEmails = []; // Tableau pour stocker les adresses e-mail

// const token = "YOUR_FCM_TOKEN"; // Remplacez par le token du destinataire
// Configuration de Socket.IO
io.on("connection", (socket) => {
  console.log("connected");

  socket.on("disconnect", () => {
    console.log("disconnected");
  });

  // Gestionnaire pour la connexion de l'utilisateur
  socket.on("user_connection", (data) => {
    const { email, token } = data;
    console.log(`Utilisateur connecté : Email - ${email}, Token FCM - ${token}`);
    //userTokens.push(token);

    userEmails.push(email);
    userTokens.push(token);

    // Exemple : stockez-les dans un tableau
    const userInformation = {
      email,
      token,
    };
    // Stockez cet objet dans une structure appropriée, par exemple, un tableau
    // users.push(userInformation);

    // Envoie une notification de bienvenue à l'utilisateur
    const welcomeNotification = {
      title: "Bienvenue sur l'application",
      body: "Vous êtes maintenant connecté !",
    };
    sendNotificationToUser(userInformation, welcomeNotification);
  });

  // Le reste de votre code pour la gestion des messages reste inchangé
  // ...
});

// Définissez la tâche planifiée pour s'exécuter toutes les 10 secondes
cron.schedule('*/10 * * * * *', () => {
  console.log(userTokens[0]);
  console.log(userEmails[0]);
  const tok = userTokens[0]+"";
  // Utilisez le token FCM du destinataire et envoyez une notification
  axios.post("https://telusholding.cloud/Mobile_test/notif.php", {
    email: userEmails[0],
  })
  .then((response) =>  {
    const responseData = response.data;
    const messagea = responseData.message;
    const notification = {
      title: "New notif",
      body: messagea,
    };

    if (messagea === 'vide') {
      console.log("vide");
      //sendNotificationToUser({ email: userEmails[0], token: tok }, notification);

    } else {
      console.log("success");

      // Envoyez la notification FCM ici, si nécessaire
      sendNotificationToUser({ email: userEmails[0], token: userTokens[0] }, notification);

    }
  })
  .then((error) => console.log(error));
  
});


// Fonction pour envoyer une notification à un utilisateur spécifique
function sendNotificationToUser(userInformation, notification) {
  const message = {
    data: {
      title: notification.title,
      body: notification.body,
    },
    notification: {
      title: notification.title,
      body: notification.body,
      //action: "FLUTTER_NOTIFICATION_CLICK", // définir l'URL de deep linking pour l'écran individuel
      
    },
    token: userInformation.token, // Utilisez le token FCM de l'utilisateur cible
  };

  admin.messaging().send(message)
    .then((response) => {
      console.log('Notification envoyée avec succès à l\'utilisateur:', userInformation.email);
    })
    .catch((error) => {
      console.error('Erreur lors de l\'envoi de la notification à l\'utilisateur:', userInformation.email, error);
    });
}





server.listen(3000, () => {
  console.log("Server started");
});
